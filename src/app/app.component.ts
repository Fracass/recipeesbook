import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  currentNavbarItem: string = 'recipes';

  onNavbarClicked(item: string) {
    this.currentNavbarItem = item;    
  }
}
