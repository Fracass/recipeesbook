import { Injectable } from "../../../node_modules/@angular/core";
import { Http, Response } from "../../../node_modules/@angular/http";
import { RecipeService } from "../recipes/recipe.service";
import { Recipe } from "../recipes/recipe.model";
import { map } from 'rxjs/operators';

@Injectable()
/** Класс-сервис доступа к данным БД Firebase */
export class DataStorageService{
    constructor(private http: Http, private recipeService: RecipeService) {}

    /**
    * Сохраняет все Рецепты в БД
    */
    saveToDb(){
        this.http.put('https://ng-recipebook-f2f7d.firebaseio.com/recipes.json', this.recipeService.getAllRecipes()).subscribe(
            (response) => console.log(response),
            (error) => console.log(error));
    }

    /**
    * Получает Рецепты из БД 
    */
    fetchFromDb() {
        //получаем json объект и преобразуем в массив объектов модели Рецептов
        this.http.get('https://ng-recipebook-f2f7d.firebaseio.com/recipes.json')
        .pipe(map(
            //инициализируем свойство ingredients пустым массивом, если его нету в бд, 
            //чтобы впоследствии не получить ошибку при работе с Ингредиентами
            (response: Response) => {
                const recipes : Recipe[] = response.json(); 
                for(let recipe of recipes) {
                    if(!recipe['ingredients']) {
                        console.log(recipe);
                        recipe['ingredients'] = [];
                    }
                }

                return recipes;
            }
        ))
        //вызываем методы сервиса Рецептов, чтобы обновить данные на стороне клиента
        .subscribe(
            (recipes: Recipe[]) => {                
                this.recipeService.updateRecipes(recipes);
                this.recipeService.recipesChanged.next(recipes);
            }
        );
    }
}