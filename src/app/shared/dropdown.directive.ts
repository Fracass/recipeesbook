import { Directive, HostListener, HostBinding } from "../../../node_modules/@angular/core";

@Directive({
    selector: '[appDropdown]'
})
/** Директива, используется для Drop-down элементов */
export class DropdownDirective{  

    @HostListener('click') toggleOpen(){
        this.isOpen = !this.isOpen;
    }

    /** Наличие класса open на dom-Элементе определяется значением переменной isOpen */
    @HostBinding('class.open') isOpen = false;
}