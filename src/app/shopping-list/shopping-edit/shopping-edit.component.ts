import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Ingredient } from '../../shared/ingredient.model';
import { ShlistService } from '../shlist.service';
import { NgForm } from '../../../../node_modules/@angular/forms';
import { Subscription } from '../../../../node_modules/rxjs';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  editMode = false;
  editedItemIndex: number;
  @ViewChild('f') ingredientForm: NgForm;

  constructor(private slService: ShlistService) { }

  ngOnInit() {
    this.subscription = this.slService.startedEditing.subscribe(
      (index:number)=> {
        this.editMode = true;
        this.editedItemIndex = index;
        const itemToEdit = this.slService.ingredients[index];

        this.ingredientForm.setValue({
          'name': itemToEdit.name,
          'amount': itemToEdit.amount
        })
      }
    )
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onSubmit(shoppingListForm: NgForm){
    let newINgredient = new Ingredient(shoppingListForm.value.name, shoppingListForm.value.amount);

    if(this.editMode){
      this.slService.updateIngredient(this.editedItemIndex, newINgredient);
    }
    else {
      this.slService.addIngredient(newINgredient);
    }

    this.editMode = false;
    shoppingListForm.reset();
  }

  onClear(){
    this.ingredientForm.reset();
    this.editMode = false;
  }

  onDeleteItem(){
    this.slService.deleteIngredient(this.editedItemIndex);
    this.onClear();
  }
}
