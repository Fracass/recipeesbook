import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Recipe } from '../../recipe.model'

@Component({
    selector: 'app-recipe-item',
    templateUrl: './recipeItem.component.html'
})
export class RecipeItemComponent{  
   @Input() recipe: Recipe;
   @Input() index: number;
   @Output() recipeSelected: EventEmitter<void> = new EventEmitter<void>();

    constructor() {}
}