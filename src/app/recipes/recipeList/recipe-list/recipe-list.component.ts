import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import {Recipe} from '../../recipe.model';
import { RecipeService } from '../../recipe.service';
import { Router, ActivatedRoute } from '../../../../../node_modules/@angular/router';
import { Subscription } from '../../../../../node_modules/rxjs';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  recipes: Recipe[];

  constructor(private recipesService: RecipeService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.recipes = this.recipesService.getAllRecipes();

    //from DB
    // this.subscription = this.recipesService.getRecipesFromDb().
    //   subscribe((recipes: Recipe[]) => {
    //     this.recipes = recipes;
    //   });
    this.subscription = this.recipesService.recipesChanged.subscribe(
      (recipes: Recipe[]) => {
        this.recipes = recipes;
      }
    );

    console.log(this.recipes);
  }

  onNewRecipe(){
    this.router.navigate(['new'], { relativeTo : this.route})
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
