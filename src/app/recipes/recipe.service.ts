import { Recipe } from "./recipe.model";
import { Ingredient } from "../shared/ingredient.model";
import { Injectable, EventEmitter } from "../../../node_modules/@angular/core";
import { ShlistService } from "../shopping-list/shlist.service";
import { Http, Response } from "@angular/http";
import { map } from "rxjs/operators";

@Injectable()
export class RecipeService{
    private recipes: Recipe[] = [
        new Recipe('Schnitzel', 
            'The term is most commonly used to refer to meats coated with flour, beaten eggs and bread crumbs',
            'https://lh3.googleusercontent.com/WePLa6gulCpK4DZzCUmxFL9dscR7VCnuf4LMfgugFH-uh65q9QGvMKXAlPbw1mA0S5Yrc1SeqRrvbEVvlRKRcpwlOuVN_0PwNieBaE0=w600-l68',
        [new Ingredient('Meat piece', 1), new Ingredient('coc', 20)]),
        new Recipe('Big Fat Burger', 
            'Just too fat', 
            'https://www.seriouseats.com/recipes/images/2015/07/20150728-homemade-whopper-food-lab-35.jpg',
            [new Ingredient('Patty', 1), new Ingredient('Cheese Slice', 1), new Ingredient('Cucumber', 4)])
    ];

    recipeSelected = new EventEmitter<Recipe>();
    recipesChanged = new EventEmitter<Recipe[]>();

    constructor(private shListService : ShlistService,
                private http: Http){}

    getAllRecipes(){
        return this.recipes.slice();
    }

    getRecipe(id: number){
        return this.recipes[id];
    }

    addIngredientsToShList(ingredients: Ingredient[]){
        this.shListService.addIngredients(ingredients);
    }

    updateRecipe(id: number, newRecipe: Recipe){
        this.recipes[id] = newRecipe;
        this.recipesChanged.next(this.recipes.slice());
    }

    updateRecipes(recipes: Recipe[]){
        this.recipes = recipes;
    }

    addRecipe(newRecipe: Recipe){
        this.recipes.push(newRecipe);
        this.recipesChanged.next(this.recipes.slice());
        console.log(this.recipes);
    }

    deleteRecipe(index: number){
        this.recipes.splice(index, 1);
        this.recipesChanged.next(this.recipes.slice());
    }

    getRecipesFromDb() {
        return this.http.get('https://ng-recipebook-f2f7d.firebaseio.com/recipes.json')
        .pipe(map((response: Response) => <Recipe[]>response.json()))
    }

    getRecipeFromDb(id : number) {
            return this.http.get('https://ng-recipebook-f2f7d.firebaseio.com/recipes/' + id)
            .pipe(map((response: Response) => <Recipe>response.json()))
    }
}
